import fetchWrapper from "@/utils/axios/fetch-wrapper";
import { formatCurrency, formatDate } from "@/utils/helper";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
(<any>pdfMake).addVirtualFileSystem(pdfFonts);

export const onGenPDF = async (data: any, updatePayment: any) => {
  try {
    console.log(data);
    const items = data?.items;
    const bodyContent: Object[] = [];

    if (items && items.length > 0) {
      items.forEach((item: any, index: any) => {
        const { itemName, itemPrice, itemAmount, totalPrice } = item;

        const total = itemPrice * itemAmount;

        bodyContent.push([
          {
            stack: [
              itemName,
              {
                text:
                  itemAmount +
                  " x " +
                  (itemPrice ? "Rp. " + formatCurrency(itemPrice) : "Rp. 0"),
              },
            ],
            margin: [6, 2, 0, 3],
          },
          {
            stack: [
              " ",
              {
                text: "Rp. " + formatCurrency(totalPrice),
                alignment: "right",
                margin: [0, 2, 6, 3],
              },
            ],
          },
        ]);
      });
    }

    const docDefinition = {
      content: [
        {
          layout: "noBorders",
          table: {
            widths: ["100%"],
            body: [
              [
                {
                  text: "Nova Beauty Salon & Spa",
                  fontSize: 12,
                  bold: true,
                  alignment: "center",
                  border: [false, false, false, true],
                  margin: [0, 0, 0, -3],
                },
              ],
            ],
          },
        },
        // Alamat
        {
          stack: [
            {
              text: "Jl. Raya Cangkudu - Cisoka, Cibugel, Kec. Cisoka, Kab. Tangerang, Banten - 15730 ",
              fontSize: 7,
              alignment: "center",
              margin: [3, 1, 3, -1],
            },
            {
              text: "+62 813-8647-0818",
              fontSize: 6,
              alignment: "center",
              margin: [3, 1, 3, 3],
            },
          ],
        },

        // Spacing
        {
          canvas: [
            {
              type: "line",
              x1: 0,
              y1: 0,
              x2: 800,
              y2: 0,
              dash: { length: 2, space: 1 },
              lineWidth: 0.2,
            },
          ],
        },

        // Invoice
        {
          layout: "noBorders",
          fontSize: 8,
          table: {
            widths: ["*", "*"],
            body: [
              [
                { text: "Nama Pelanggan", bold: true, margin: [6, 0, 0, 0] },
                {
                  text: data?.customerName,
                  alignment: "right",
                  margin: [0, 0, 6, 0],
                },
              ],
            ],
          },
        },
        {
          layout: "noBorders",
          fontSize: 8,
          table: {
            widths: ["*", "65%"],
            body: [
              [
                {
                  text: "No. Invoice",
                  bold: true,
                  border: [false, true, false, false],
                  margin: [6, -3, 0, 0],
                },
                {
                  text: "#" + data?.invoice,
                  alignment: "right",
                  border: [false, true, false, false],
                  margin: [0, -3, 6, 0],
                },
              ],
            ],
          },
        },
        {
          layout: "noBorders",
          fontSize: 8,
          table: {
            widths: ["*", "65%"],
            body: [
              [
                { text: "Tanggal", bold: true, margin: [6, -3, 0, 2] },
                {
                  text: formatDate(data?.paymentDate, "DD MMMM YYYY"),
                  alignment: "right",
                  margin: [0, -3, 6, 0],
                },
              ],
            ],
          },
        },

        // Spacing
        {
          canvas: [
            {
              type: "line",
              x1: 0,
              y1: 0,
              x2: 800,
              y2: 0,
              dash: { length: 2, space: 1 },
              lineWidth: 0.2,
            },
          ],
        },

        // service
        {
          fontSize: 8,
          layout: "noBorders",
          table: {
            headerRows: 1,
            widths: ["50%", "*"],
            body: [...bodyContent.map((item) => Object.values(item))],
          },
        },

        // Spacing
        {
          canvas: [
            {
              type: "line",
              x1: 0,
              y1: 0,
              x2: 800,
              y2: 0,
              dash: { length: 2, space: 1 },
              lineWidth: 0.2,
            },
          ],
        },

        // Meta Price
        {
          fontSize: 8,
          layout: "noBorders",
          table: {
            headerRows: 1,
            widths: ["*", "*"],
            body: [
              [
                {
                  text: "Metode :",
                  bold: true,
                  alignment: "left",
                  margin: [6, 0, 0, -2],
                },
                {
                  text: updatePayment?.paymentMethod || data?.paymentMethod,
                  alignment: "right",
                  margin: [0, 0, 6, -2],
                },
              ],
              [
                {
                  text: "Total Qty:",
                  bold: true,
                  alignment: "left",
                  margin: [6, 0, 0, -2],
                },
                {
                  text: data?.totalAmount,
                  alignment: "right",
                  margin: [0, 0, 6, -2],
                },
              ],
              [
                {
                  text: "Sub Total :",
                  bold: true,
                  alignment: "left",
                  margin: [6, 0, 0, -2],
                },
                {
                  text: "Rp. " + formatCurrency(data?.totalPrice),
                  alignment: "right",
                  margin: [0, 0, 6, -2],
                },
              ],
              [
                {
                  text: "Point :",
                  bold: true,
                  alignment: "left",
                  margin: [6, 0, 0, -2],
                },
                {
                  text: data?.totalPoint,
                  alignment: "right",
                  margin: [0, 0, 6, -2],
                },
              ],
              [
                {
                  text: "Bayar :",
                  bold: true,
                  alignment: "left",
                  margin: [6, 0, 0, -2],
                },
                {
                  text:
                    data?.paymentAmount !== 0
                      ? "Rp. " + formatCurrency(data?.paymentAmount)
                      : "Rp. 0",
                  alignment: "right",
                  margin: [0, 0, 6, -2],
                },
              ],
              [
                {
                  text: "Kembali :",
                  bold: true,
                  alignment: "left",
                  margin: [6, 0, 0, -2],
                },
                {
                  text:
                    data?.changeAmount !== 0
                      ? "Rp. " + formatCurrency(data?.changeAmount)
                      : "Rp. 0",
                  alignment: "right",
                  margin: [0, 0, 6, 2],
                },
              ],
            ],
          },
        },

        // Spacing
        {
          canvas: [
            {
              type: "line",
              x1: 0,
              y1: 0,
              x2: 800,
              y2: 0,
              dash: { length: 2, space: 1 },
              lineWidth: 0.2,
            },
          ],
        },
        {
          stack: [
            {
              fontSize: 8,
              text: "Terima Kasih!",
              alignment: "center",
              margin: [0, 2, 0, 0],
            },
          ],
        },
      ],
      pageMargins: [-3, 0, 2, 5],
      pageSize: {
        width: 150,
        // width: '4 932.28346',
        // // height: 725,
        height: "auto",
      },
    };

    const pdf = pdfMake.createPdf(docDefinition);

    pdf.getBlob(async (blob: Blob) => {
      const formData = new FormData();
      formData.append("file", blob, `${data?.invoice}.pdf`);
      try {
        await fetchWrapper.post(
          `transaction/saveInv/${data?.paymentCode}`,
          formData
        );
      } catch (error) {
        console.error("Gagal mengirim file PDF:");
      }
    });

    pdf.open();

    return pdf;
  } catch (error) {
    console.error("Terjadi kesalahan:", error);
  }
};
